const express = require('express')
const app = express();
const fs = require('fs');
const { networkInterfaces } = require('os');

const cursos = [
    {
        id: 1,
        nombre: "electricidad"
    },
    {
        id: 2,
        nombre: "programacion"
    },
    {
        id: 3,
        nombre: "musica"
    },
    {
        id: 4,
        nombre: "pintura"
    },
    {
        id: 5,
        nombre: "peluqueria"
    }
]


const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);

    next();
}

app.use(logger)

const esAdministrador = (req, res, next) => {
    const usuarioAdministrador = true;
    if(usuarioAdministrador) {
        console.log('El usuario esta esta correctamente logueado');
        next();
    }
    else {
        res.send('No esta logueado')
    }

};

const informacionEnLog = (req, res, next) => {
    fs.appendFile('log.txt', `${req.path}`, err => {
        if(err) throw err;
        
        next()
        
    })
}

app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
        { id: 1, nombre: "Lucas", edad: 35}
    ])
});

app.post('/estudiantes', (req, res) => {
    res.status(201).send();
})

app.get('/Cursos', informacionEnLog, (req, res) => {
    res.json(cursos)
})





app.listen( 3000, () => console.log("servidor corriendo en el puerto 3000"))