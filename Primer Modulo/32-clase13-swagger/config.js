require('dotenv').config();

const config = { 
    port : process.env.NODE_PORT || 3000 
}

module.exports = config