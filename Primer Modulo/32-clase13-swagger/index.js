const config = require('./config');
const marcasAutos = require('./data');
const express = require('express')
const app = express();
const cors = require('cors')

app.use(cors())

function validarIdAuto(req, res, next){
    const idAuto = parseInt(req.params.id);  
    
    if( !Number.isInteger(idAuto) ){
        res.status(422).json({msg : "El id debe ser un numero entero"})
        return 
    } else{
        next()
    }
}




app.get('/', (req, res) =>{
    console.log("req.path")
    console.log(req.path)   // req.path me devuelve la /, otro es req.method que me devuelve GET
    console.log("req.method")
    console.log(req.method)
    res.status(200).send(marcasAutos)
})

app.get('/autos', (req, res) =>{
    //const auto = marcasAutos[0];       //esto es procesar informacion
    res.status(200).json(marcasAutos)
})

 //autos/1 al 1 lo convierto en un parametro para poder hacer algo mas general
//el:id me permite poder ingresar un numero como parametro para poder hacerlo gneral
//el /:id tiene que ser igual al al de req.params.id

app.get('/autos/:id', validarIdAuto, (req, res) => {     
    
    const idAuto = parseInt(req.params.id);  
    
    /* if( !Number.isInteger(idAuto) ){
        res.status(422).json({msg : "El id debe ser un numero entero"})
        return 
    } */


    //const idAuto = parseInt(req.params.id);      
    
    const auto = marcasAutos.find( auto => auto.id === idAuto)      //ddevuleve un objeto
    
    if(auto) {
        res.status(200).json(auto);
    } else{
        res.status(400).json(`No se encuentra el auto con id ${idAuto}`)
    }

    //const auto = marcasAutos[idAuto-1];       

    
})

app.delete('/autos/:id', validarIdAuto, (req, res) => {

    //obtenemos el id de la ruta
    const idAuto = parseInt(req.params.id);  
    
    /* //verificamos el input
    if( !Number.isInteger(idAuto) ){
        res.status(422).json({msg : "El id debe ser un numero entero"})
        return 
    } */
    
    //buscamos si el auto existe
    const auto = marcasAutos.find( auto => auto.id === idAuto)   //esto me devulve todos los autos menos el id
    
    //si no existe damos un error
    if(!auto) {
        res.status(400).json(`No se encuentra el auto con id ${idAuto}`)
        return
    }

    //eliminar el auto
    const posAutoId = marcasAutos.indexOf(auto)

    marcasAutos.splice(posAutoId, 1)

    res.json(marcasAutos)


})




app.post('/autos', (req, res) =>{
    console.log("req.path")
    console.log(req.path) 
    console.log("req.method")
    console.log(req.method)
    res.status(201).send("Ruta para crear un auto")
})


app.listen(config.port, () =>{
    console.log(`Servidor corriendo en el puerto ${config.port}`)
})