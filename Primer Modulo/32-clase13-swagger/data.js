/* const autos = [
    "mercedes",
    "bmw",
    "audi"
] */

const marcasAuto = [
    {
        id: 1,
        nombre: "Fiat",
        modelo: 2010,
    }, 
    {
        id: 2,
        nombre: "Mercedes",
        modelo: 2015,
    }, 
]

module.exports = marcasAuto