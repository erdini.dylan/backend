class Usuario{
    constructor(nombre, apellido, email, pais, contrasenia, contrasenia2){
        this.nombre = nombre;
        this.apellido=apellido;
        this.email=email;
        this.pais=pais;
        this.contrasenia=contrasenia;
        this.contrasenia2=contrasenia2;
    }
}

let vectorUsuarios = []

document.getElementById("btn-registrarse").addEventListener("click", function() {
    const nombre         = document.getElementById("nombre").value;
    const apellido       = document.getElementById("apellido").value;
    const email          = document.getElementById("email").value;
    const pais           = document.getElementById("pais").value;
    const contrasenia    = document.getElementById("contrasenia").value;
    const contrasenia2   = document.getElementById("contrasenia2").value;

    
    if(nombre === "" || apellido === "" || email === "" || pais === "" || contrasenia === "" ){
        alert("Los datos no pueden estar vacios")
        return;
    }

    if(contrasenia != contrasenia2){
        alert("Las Contraseñas deben ser iguales");
        return;
    }
    
    for(let i=0; i<vectorUsuarios.length; i++){
        if(email === vectorUsuarios[i].email){
            alert("El email ya esta en uso, prueba con otro");
            return;
        }
    }

    let nuevoUsuario = new Usuario(nombre, apellido, email, pais, contrasenia, contrasenia2)
        vectorUsuarios.push(nuevoUsuario);
         console.log(vectorUsuarios)

})

document.getElementById("btn-login").addEventListener("click", function(){
    const email = document.getElementById("email-login").value;
    const contrasenia = document.getElementById("contrasenia-login").value;
    let band = false;
    let pos = 0;
    let i = 0;

    if(email === "" || contrasenia === "" ){
        alert("Los datos no pueden estar vacios")
        return;
    }

    for(i=0; i<vectorUsuarios.length; i++){
        if(email === vectorUsuarios[i].email && contrasenia === vectorUsuarios[i].contrasenia){
            band = true;
            pos = i;
        }
    }
    if(band === true){
        alert("Inicio sesion exitosamente. Indice: " + pos)
        return;
    } else{
        alert(false);
        return;
    }
})