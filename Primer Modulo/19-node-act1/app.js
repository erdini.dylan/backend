const fs = require('fs')

const calculadora = require("./calculator");

num1 = 5;
num2 = 5;

const suma = calculadora.sumar(num1, num2);
const resta = calculadora.restar(num1, num2);
const multiplicacion = calculadora.multiplicar(num1, num2);
const division = calculadora.dividir(num1, num2);

let texto = `Suma: ${suma}, Resta: ${resta}, Multiplicacion: ${multiplicacion}, Division: ${division}`

fs.appendFile("log.txt", texto, (err) => {
    if(err){
        console.log(err)
    } else{
        console.log("Guardado")
    }
})

