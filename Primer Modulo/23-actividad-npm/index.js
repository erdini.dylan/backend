const coolImages = require('cool-images')
const fs = require('fs')
const moment = require('moment')
moment.locale('es')

const arrayDeImagenes = coolImages.many(600, 800, 10);


arrayDeImagenes.forEach( imagen => {
    fs.appendFile("imagenes.txt", `${imagen} guardada el dia: ${moment().format('LLLL')}\n`, err => {
        if(err) throw err;
    })
})

function mostrarUrls(){
    fs.readFile("imagenes.txt", (error, datos) =>{
        if(error) throw error;
        else console.log(datos.toString())
    })
}

mostrarUrls()