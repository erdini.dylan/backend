const express = require('express')
const app = express()

const telefono = [
    {
        Marca : "samsung",
        Gama: "alta",
        Modelo: "2020",
        Pantalla: "5.5 pulgadas",
        Sist_op: "android",
        Precio: 10000,
    },
    {
        Marca : "philips",
        Gama: "baja",
        Modelo: "2010",
        Pantalla: "5.6 pulgadas",
        Sist_op: "android",
        Precio: 2000,
    },
    {
        Marca : "lg",
        Gama: "alta",
        Modelo: "2021",
        Pantalla: "5.4 pulgadas",
        Sist_op: "android",
        Precio: 3000,
    },
    {
        Marca : "huawei",
        Gama: "baja",
        Modelo: "2013",
        Pantalla: "5.5 pulgadas",
        Sist_op: "android",
        Precio: 50,
    },
    {
        Marca : "apple",
        Gama: "alta",
        Modelo: "2021",
        Pantalla: "5.8 pulgadas",
        Sist_op: "IOS",
        Precio: 55000,
    }
]


//array del objeto telefono
app.get('/', (req, res) => {
    res.json(telefono)
})

const mitad = (telefono.length/2)
const mitadCaracteristicas = [];

for(let i = 0;i<mitad ;i++){
    mitadCaracteristicas.push(telefono[i])
}

//primera mitad del array del objeto telefono
app.get('/mitad', (req, res) => {
    res.json(mitadCaracteristicas)
})

//endpoint del telefono mas caro
let telefonoMasCaro = telefono[0]
app.get('/maxPrecio', (req, res) => {
    for(let i = 0; i<telefono.length ;i++){
        if(telefono[i].Precio > telefonoMasCaro.Precio){
            telefonoMasCaro = telefono[i]
        }
    }
    res.json(telefonoMasCaro)
})

// endpoint del telefono mas barato
let telefonoMasBarato = telefono[0]
app.get('/minPrecio', (req, res) => {
    for(let i = 0; i<telefono.length ;i++){
        if(telefono[i].Precio < telefonoMasCaro.Precio){
            telefonoMasBarato = telefono[i]
        }
    }
    res.json(telefonoMasBarato)
})

//endpoint con las gamas filtradas
app.get('/filtradoGamas', (req, res) => {

    const gamaBaja = telefono.filter( (elemento) => {
        return elemento.Gama === 'baja';
    })

    const gamaMedia = telefono.filter((elemento) =>{
        return elemento.Gama === 'media';
    })

    const gamaAlta = telefono.filter((elemento) => {
        return elemento.Gama === 'alta';
    })

    const gamas = { 
        GamaBaja: gamaBaja,
        GamaMedia: gamaMedia,
        GamaAlta: gamaAlta,
    }

    res.json(gamas)
})



app.listen(3000, () => {
    console.log("puerto 3000")
})