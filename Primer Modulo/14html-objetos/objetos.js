const miAuto = {
    marca: "Peugeot",
    modelo: "Partner",
    anio: 2020,
    dominio: "AB123CD",
    _alquilado: false,          //el guion bajo significa que la porpiedad de mi atributo es privada. en js no se puede hacer privado pero funciona como una convencion
    alquilar: function(){
        if(this._alquilado){
            throw new Error("No se puede alquilar")         //puedo poner mis propios errores
        }
        this._alquilado = true;
    },
    devolver: function() {
        this._alquilado = false;
        return this._alquilado
    }
}

const autoDeDolores = {
    marca: "Ford",
    modelo: "Fiesta",
    anio: 2020,
    dominio: "AB124CD",
    alquilado: false,
    alquilar: function(){
        this.alquilado = true;
    }
}

//puedo poner una funcion dentro de los objetos para que se muestren solos

const garage = [miAuto, autoDeDolores];


function publicarAuto(auto) {
    console.log(`Se vende el auto marca: ${auto.marca}, modelo: ${auto.modelo}, año de fabricacion: ${auto.anio}`)
}

do {
    
    let nuevoAuto = {}
    nuevoAuto.marca = prompt("Ingrese la marca de su auto")
    nuevoAuto.modelo = prompt("Ingrese el modelo de su auto")
    nuevoAuto.anio = prompt("Ingrese el año de su auto")
    nuevoAuto.dominio = prompt("Ingrese el dominio de su auto")
    nuevoAuto._alquilado = false
    garage.push(nuevoAuto)

} while (window.confirm("Desea seguir agregando autos al garage?"));           //window es un objeto global que tiene metodos como una libreria, un metodo es confirmar

for(let i = 0; i < garage.length; i++){
    const autoDelGarage = garage[i];

    publicarAuto(autoDelGarage);

}

