const chalk = require('chalk');
const fs = require('fs')
const env = require('/Users/erdin/OneDrive/Escritorio/Acamica/backend/21-var-entorno/env.variables.json')


let nombre = "Dylan"

console.log(`Mi nombre es ${chalk.bgRed(nombre)}`)

fs.readdir("./", function (err, files) {
    files.forEach(file => {
        console.log(`${chalk.green(file)}`)
    })
})

let node_env = process.env.node_env || "production";

let variables = env[node_env];

console.log(`Estamos usando el puerto ${chalk.red(variables.PORT)}`)