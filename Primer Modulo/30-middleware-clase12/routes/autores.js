const express = require('express')
const router = express.Router()
let dataAutores = require('../data')
const middle = require('../middlewares/autores')


router.get('/', (req, res) => {
    res.status(200).json({"Autores" : dataAutores})
})

const longitudArray = dataAutores.length
router.post('/', (req, res) =>{

    dataAutores.push(req.body)

    if(dataAutores.length === longitudArray){
        res.status(500).json('Se produjo un error con la creacion del autor')
    } else{
        res.status(200).json("Autor Creado")
    }

})

router.get('/:id', middle.validar_params_autor, (req, res) => {

    const autor = req.autor
    res.status(200).json(autor)

})

router.delete('/:id', middle.validar_params_autor, (req, res) => {

    const autor = req.autor
    const indiceAutor = dataAutores.indexOf(autor)

    dataAutores.splice(indiceAutor, 1)
    res.status(200).json("Autor Eliminado")

})

router.put('/:id', middle.validar_params_autor, (req, res) => {

    const autor = req.autor
    const indiceAutor = dataAutores.indexOf(autor)
    dataAutores[indiceAutor] = req.body

    res.status(200).json("Autor modificado")

})

//LIBROS

router.get('/:id/libros', middle.validar_params_autor, (req, res) => {

    const autor = req.autor
    res.status(200).json({ "Libros del autor" : autor.libros})

})

router.post('/:id/libros', middle.validar_params_autor, (req, res) => {
    
    const autor = req.autor
    const indiceAutor = dataAutores.indexOf(autor)
    
    dataAutores[indiceAutor].libros.push(req.body)
    res.status(200).json("Libro añadido al autor con exito")
})

router.get('/:id/libros/:idLibro', middle.validar_params_autor, middle.validar_params_libro, (req, res)=> {

    const libro = req.libro
    res.status(200).json({"Libro" : libro})

})

router.put('/:id/libros/:idLibro', middle.validar_params_autor, middle.validar_params_libro, (req, res) => {

    const autor = req.autor
    const libro = req.libro
    const indiceAutor = dataAutores.indexOf(autor)
    const indiceLibro = autor.libros.indexOf(libro)
    dataAutores[indiceAutor].libros[indiceLibro] = req.body
    res.status(200).json("Libro modificado con exito")

})

router.delete('/:id/libros/:idLibro', middle.validar_params_autor, middle.validar_params_libro, (req, res) =>{

    const autor = req.autor
    const libro = req.libro
    const indiceAutor = dataAutores.indexOf(autor)
    const indiceLibro = autor.libros.indexOf(libro)
    dataAutores[indiceAutor].libros.splice(indiceLibro, 1)
    res.status(200).json("Libro eliminado con exito")


})

module.exports = router;