const express = require('express')
const router = express.Router()
let dataAutores = require('../data')

function validar_params_autor(req, res, next) {
    const idParams = parseInt(req.params.id)
    const autor = dataAutores.find(elemento => elemento.id === idParams)

    if(!Number.isInteger(idParams)){
        res.status(422).json({"msg" : "El id debe ser un numero entero"})
    } else{
        if(!autor){
            res.status(400).json("Id no encontrado")
            return
        }
    }
    req.autor = autor
    next()

}

function validar_params_libro(req, res, next) {
    const idLibroParams = parseInt(req.params.idLibro)
    const autor = req.autor
    const libro = autor.libros.find(elemento => elemento.id === idLibroParams)

    if(!Number.isInteger(idLibroParams)){
        res.status(422).json({"msg" : "El id del libro debe ser un numero entero"})
    } else{
        
        if(!libro){
            res.status(400).json("Id del libro no encontrado")
            return
        }
    }
    req.libro = libro
    next()

}

module.exports = {validar_params_autor, validar_params_libro}