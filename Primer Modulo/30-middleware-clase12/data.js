const autores = [
    {
        id: 1,
        nombre: "Jose Luis",
        apellido: "Borges",
        fecha: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus mas grandes...",
                anio: 1944
            },
            {
                id: 2,
                titulo: "Accion",
                descripcion: "Otra recopilacion de cuentos...",
                anio: 1949
            },
        ]
    },
    {
        id: 2,
        nombre: "Dylan",
        apellido: "Erdini",
        fecha: "10/07/2000",
        libros: [
            {
                id: 1,
                titulo: "Aprendiendo",
                descripcion: "Se trata de un libro donde el autor aprende a programar",
                anio: 2019
            },
            {
                id: 2,
                titulo: "Aprendiendo segunda edicion",
                descripcion: "Otra recopilacion de cuentos sobre el autor aprendiendo a programar",
                anio: 2020
            },
        ]
    },
    {
        id: 3,
        nombre: "Antonela",
        apellido: "Piaggio",
        fecha: "10/07/2000",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus mas grandes logros",
                anio: 1944
            },
            {
                id: 2,
                titulo: "Accion",
                descripcion: "Otra recopilacion de cuentos",
                anio: 1949
            },
        ]
    }
]

module.exports = autores;