const express = require('express');
const app = express();

app.use(express.json())

const autores = require('./routes/autores')
app.use('/autores', autores)

app.listen(5000, () => console.log("Servidor corriendo en el puerto 5000"))