const express = require('express')
const app = express();

const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);

    next();
}

app.use(logger)

const esAdministrador = (req, res, next) => {
    const usuarioAdministrador = true;
    if(usuarioAdministrador) {
        console.log('El usuario esta esta correctamente logueado');
        next();
    }
    else {
        res.send('No esta logueado')
    }

};


app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
        { id: 1, nombre: "Lucas", edad: 35}
    ])
});

app.post('/estudiantes', (req, res) => {
    res.status(201).send();
})


app.listen( 3000, () => console.log("servidor corriendo en el puerto 3000"))