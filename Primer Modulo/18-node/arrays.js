const mascotas = ["perro", "gato", "pez"];

mascotas.push("pajaro");

let titulo = "Mis Mascotas";


const mostrarMascotas = (listadoDeMascotas) => {
    
    let ejemplo = "El valor de un ejemplo"

    console.log(titulo)
    console.log(ejemplo)
    
    
    listadoDeMascotas.forEach(function (mascota, idx){
        console.log(`${idx} - ${mascota}`)
    })                                                          //lo que hace el forEach es aplicarle una funcion a cada elemento
                                                   //puedo hacer la funcion flecha dentro del forEach
                                                    //no tiene sentido ponerle un nombre a la funcion que esta dentro del foreach
                                                    // podemos pedir indice (idx) dentro de la funcion que es de cada elemento


}



/*const mostrarMascotas = (listadoDeMascotas) => {
    for(let i = 0; i<listadoDeMascotas.length; i++){
        console.log(`${i} - ${listadoDeMascotas[i]}`)
    }
}*/

mostrarMascotas(mascotas)

