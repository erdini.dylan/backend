class Libros{
    constructor(titulo, autor, anio, genero){
        this.titulo = titulo;
        this.autor = autor;
        this.anio = anio;
        this.genero = genero;
    }
    infoLibro(){
        return `Titulo: ${this.titulo} Autor: ${this.autor} Anio: ${this.anio} Genero: ${this.genero}`;
    }
}

vectorLibros = [];

function mostrarLibros(){
    console.log(vectorLibros);
}

function ordenarAlfabeticamente(){
    
    let autores = []

    for(let i=0; i<vectorLibros.length; i++){
        autores.push(vectorLibros[i].autor);
    }
    autores.sort();
    console.log(autores)
}

function filtrarGenero(){
    generoFiltrado = []
    buscargenero = prompt("Ingrese un genero por el que quiere filtrar: ");
    for(let i=0; i<vectorLibros; i++){
        if(buscargenero == vectorLibros[i].genero){
            generoFiltrado.push(vectorLibros[i].genero)
        }
    }
}

document.getElementById("btn-enviar").addEventListener("click", function(){
    const titulo = document.getElementById("titulo").value;
    const autor = document.getElementById("autor").value;
    const anio = document.getElementById("anio").value;
    const genero = (document.getElementById("genero").value).toLowerCase();

    if(titulo === "" || autor === "" || anio === "" || genero === ""){
        alert("No puede haber ningun campo vacio");
        return;
    }
    if(!isNaN(anio) && anio.length != 4){
        alert("El año tiene que ser numeral y no puede superar los 4 digitos");
        return;
        
    }

    if(genero != "aventura" && genero != "terror" && genero != "fantasia"){
        alert("El genero solo puede ser de aventura, terror o fantasia");
        return;
    }

    nuevoLibro = new Libros(titulo, autor, anio, genero);
    vectorLibros.push(nuevoLibro)
    mostrarLibros()
    ordenarAlfabeticamente()
})

