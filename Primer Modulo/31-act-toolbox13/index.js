let express = require('express');
let app = express()
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUI = require('swagger-ui-express')




class Estudiante {
    constructor(legajo, nombre, apellido, edad){
        this.legajo = legajo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    datos(){
        console.log("Legajo: " + this.legajo + " Nombre: " + this.nombre + " Apellido: " + this.apellido + " Edad: " + this.edad);
    }

}

array = [];

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0'
        }
    },
    apis: ['./index.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs))


app.get('/companieros', function(req, res){

    console.log(array)
    res.send(array);
    
})

/** 
 * @swagger
 * /companieros:
 *    post:
 *          description: Crea un nuevo estudiante
 *          parameters:
 *          - name: nombre
 *            description: Nombre del estudiante
 *            in: formData
 *            required: true
 *            type: string
 *          - name: edad
 *            description: Edad del estudiante
 *            in: formData
 *            required: true
 *            type: integer
 *          responses:
 *              200:
 *                  description: Success
 *
*/
app.post('/companieros', function(req, res){

    const estudianteNuevo = new Estudiante(12, "dylan", "erdini", 12);
    array.push(estudianteNuevo)
    res.status(201).send("Estudiante Agregado")
    
})

app.listen(3000, function(){
    console.log("Escuchando en el puerto 3000")
})

