const { Marca, Modelo } = require('../src/models/marcas')

async function validarLlaveForanea(req, res ,next) {
    const llave = parseInt(req.body.marcaId)
    const busqueda = await Marca.findByPk(llave)
    if(busqueda){
        next()
    } else{
        res.status(400).json({"msg": "El id de la marca ingresada no pertenece a uno existente "})
    }
}

module.exports = {
    validarLlaveForanea,
}