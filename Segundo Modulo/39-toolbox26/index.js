const express = require('express')
const app = express();
app.use(express.json());

const marcas = require('./routes/marcas')
app.use('/marcas', marcas)

const modelos = require('./routes/modelos')
app.use('/modelos', modelos)


app.listen(3000, ()=>{console.log("puerto 3000")})




/* class User extends Model{}

User.init({
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING
}, { sequelize, modelName: 'users' }
);

(async () => {
    await sequelize.sync();

    //CREAR

    const got = await User.create({
        nombre: "Jon",
        apellido: "Snow"
    });
    console.log(got.toJSON())

    //MODIFICAR

    got.nombre = "Arya";
    got.apellido = "Stark";

    await got.save();

    console.log(got.toJSON())

    //ELIMINAR

    // await got.destroy();
    
    //buscar un elemento en la base de datos

    const arya = await User.findOne({
        where: {
            nombre: "Arya"
        }
    })
    console.log(arya)

    ////buscar varios elementos en la base de datos

    const snow = await User.findAll({
        where: {
            apellido: "Snow"
        }
    })
    console.log(snow)

}) ();

 */



/* class Carac extends Model { }

Carac.init({
    pantalla: DataTypes.STRING,
    smart: DataTypes.BOOLEAN,
    precio: DataTypes.INTEGER,
    modelo_id: DataTypes.INTEGER
}, {sequelize, modelName: "Caracteristicas"}) */


//Modelo.belongsTo(Marca, {foreignKey: 'marca_id'})











/* app.get('/marcas', async (req, res) => {
    const listado = await Marca.findAll({
        include: [
            {
                model: Modelo,
                as: 'Modelos'
            }
        ]
    })
    res.status(200).json({Marca: listado})
}) 

app.get('/modelos', async (req, res) => {
    const listado = await Modelo.findAll()
    // console.log(Modelo)
    res.status(200).json({Modelo: listado})
}) 

/* app.post('/tv', (req, res) => {
    const datos = req.body
    const tv = Carac.create({
        pantalla: datos.pantalla,
        smart: datos.smart,
        precio: datos.precio
    })
    res.status(201).json({"msg": "Creado"})
}) 

app.post('/marcas', (req, res) => {
    const datos = req.body
    const nuevaMarca = Marca.create({
        nombre: datos.nombre,
    })
    res.status(201).json({"msg": "Creado"})
})

app.post('/modelos', (req, res) => {
    const datos = req.body
    const nuevoModelo = Modelo.create({
        nombre: datos.nombre,
        pantalla: datos.pantalla,
        smart: datos.smart,
        precio: datos.precio,
        marcaId: datos.marcaId
    })
    res.status(201).json({"msg": "Creado"})
})

 */

