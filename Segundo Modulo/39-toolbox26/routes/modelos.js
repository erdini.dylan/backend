const express = require('express')
const router = express.Router()
const { Marca, Modelo, Op } = require('../src/models/marcas')
const { validarLlaveForanea } = require('../middlewares/modelos')

router.get('/', async (req, res) => {
    const listado = await Modelo.findAll()
    res.status(200).json({Modelo: listado})
}) 

router.post('/', validarLlaveForanea , (req, res) => {
    const datos = req.body
    const nuevoModelo = Modelo.create({
        nombre: datos.nombre,
        pantalla: datos.pantalla,
        smart: datos.smart,
        precio: datos.precio,
        marcaId: datos.marcaId
    })
    res.status(201).json({"msg": "Modelo creado"})
})

router.get('/altos/:nro', async (req, res) => {
    const nro = parseInt(req.params.nro)
    const preciosMasAltos = await Modelo.findAll({
        where: {
            precio: {
                [Op.gt]:nro
            }
        }
    })
    res.status(200).json({Modelo: preciosMasAltos})
}) 

router.get('/bajos/:nro', async (req, res) => {
    const nro = parseInt(req.params.nro)
    const preciosMasBAjos = await Modelo.findAll({
        where: {
            precio: {
                [Op.lt]: nro
            }
        }         
    })
    res.status(200).json({Modelo: preciosMasBAjos})
}) 

router.get('/ordenados', async (req, res) => {
    const ordenados = await Modelo.findAll({
        order: [
            [
                "precio", 'ASC'
            ]
        ]
    })
    res.status(200).json({Modelos: ordenados})
}) 

module.exports = router