const express = require('express')
const router = express.Router()
const { Marca, Modelo } = require('../src/models/marcas')


router.get('/', async (req, res) => {
    const listado = await Marca.findAll({
        include: [
            {
                model: Modelo,
                as: 'Modelos'
            }
        ]
    })
    res.status(200).json({Marcas: listado})
})

router.get('/:marcaParam', async (req, res) => {
    const marcaParam = req.params.marcaParam
    const busqueda = await Marca.findOne({
        where: {
            nombre: marcaParam
        },
        include:[
            {
                model: Modelo,
                as: 'Modelos'
            }
        ]
        
    })

    res.status(200).json({Marca: busqueda})    
})

router.post('/', (req, res) => {
    const datos = req.body
    const nuevaMarca = Marca.create({
        nombre: datos.nombre,
    })
    res.status(201).json({"msg": "Marca creada"})
})



module.exports = router





