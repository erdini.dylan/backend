const { Model, DataTypes, Op} = require('sequelize')
const { sequelize } = require('../connection/sequelize')

class Marca extends Model { }

Marca.init({
    nombre: DataTypes.STRING,
}, {sequelize, modelName: "Marcas"})

class Modelo extends Model { }

Modelo.init({
    nombre: DataTypes.STRING,
    pantalla: DataTypes.STRING,
    smart: DataTypes.BOOLEAN,
    precio: DataTypes.INTEGER,
    //marca_id: Marca.id
}, {sequelize, modelName: "Modelos"})

Marca.hasMany(Modelo, {as: "Modelos", foreignKey: "marcaId"})


// ;(async() => {
//      await sequelize.sync()
// })();

module.exports = {
    Marca, 
    Modelo,
    Op
}