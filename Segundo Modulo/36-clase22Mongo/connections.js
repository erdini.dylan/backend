const mongoose = require('mongoose')
const mongoURL = "mongodb://localhost:27017/homeBanking"

mongoose.connect(mongoURL)

module.exports = mongoose