const express = require('express')
const app = express()
app.use(express.json())

const cuentas = require('./routes/cuentas')
app.use('/cuentas', cuentas)

app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000")
})