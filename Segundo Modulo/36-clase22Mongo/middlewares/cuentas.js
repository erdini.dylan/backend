const schemas = require('../models/cuentas')

async function validarEmail (req, res, next){
    
    try {
        let datosBody = req.body
        let BusqUsuario = await schemas.Cuenta.findOne({email: datosBody.email})
        if(!BusqUsuario){
            
            next()
        }else{
            console.log(BusqUsuario)
            console.log(datosBody)
            res.status(400).json({"msg":"El email ingresado ya se encuentra utilizado"})
        }
        
    } catch (error) {
        console.log(error)
        res.status(400).json({"msg":"Ocurrio un error"})
    }
}

module.exports = {
    validarEmail
}