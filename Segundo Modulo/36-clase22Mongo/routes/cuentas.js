const express = require('express')
const router = express.Router()
const middles = require('../middlewares/cuentas')
const schemas = require('../models/cuentas')

router.post('/', middles.validarEmail ,async (req, res) => {
    let { nombre, apellido, email } = req.body
    let cuenta = new schemas.Cuenta({nombre: nombre, apellido: apellido, email: email})
    let nuevaCuenta = await cuenta.save()
    res.status(201).json({"msg": "Usuario agregado con exito"})
})

router.put('/', async (req, res)=>{
    
})


module.exports = router