const mongooseConect = require('../connections')

const datosUsuarios = {
    nombre: String,
    apellido: String,
    email: String
}

const contacto = {
    direccion: String,
    telefono: Number,
    intragram: String
}

const Cuenta = mongooseConect.model("Cuenta", datosUsuarios)

module.exports = { Cuenta, contacto, datosUsuarios}