const mongooseConec = require('./connections')

const menu = {
    plato: String,
    precio: Number,
    tipo_de_plato: String
}

const Platos = mongooseConec.model("Platos", menu)

module.exports = { menu, Platos };