const schemas = require('../schemas')

async function validarId (req, res, next){
    try {
        let idParams = req.params.idPlato
        let busquedaPlato = await schemas.Platos.findById(idParams)
        if(busquedaPlato){
            next()
        } else{
            res.status(404).json({"msg": "El id ingresado no pertence a un plato"})
        }

    } catch (error) {
        console.log(error)
        res.status(422).json({"msg":"No ingreso un formato valido"})
    }
}

module.exports = { validarId }