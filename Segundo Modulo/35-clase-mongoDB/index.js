const express = require('express');
const app = express()
const mongooseConec = require('./connections');
const { validarId } = require('./middlewares/usuarios');
const schemas = require('./schemas')
app.use(express.json())


app.get('/platos', async (req, res) => {

    try{
        const platosdb = await schemas.Platos.find()
        res.status(200).json({"Platos":  platosdb})
    } catch(err){
        res.status(200).json({"mensaje":  "Error al conectarse con la base de datos"})
    }
})
app.post('/platos', async (req, res) => {
    let datosBody = req.body
    let plato = new schemas.Platos(datosBody)
    let nuevoPlato = await plato.save()

    res.status(201).json({"Plato agregado con exito": nuevoPlato})

})

app.get('/platos/:idPlato', validarId, async (req, res) => {
    let idParams = req.params.idPlato
    let busqPlato = await schemas.Platos.findById(idParams)
    res.status(200).json({"Plato": busqPlato })
})


app.put('/platos/:idPlato', validarId, async (req, res) => {
    try {
        let idParams = req.params.idPlato
        let { plato, precio, tipo_de_plato } = req.body
        let platoActualizado = await schemas.Platos.findByIdAndUpdate(idParams, {
            plato: plato,
            precio: precio,
            tipo_de_plato: tipo_de_plato 
        })

        let busqPlato = await schemas.Platos.findById(idParams)

        res.status(200).json({"Plato actualizado": busqPlato})

    } catch (error) {
        res.status(400).json({"msg":"Ocurrio un error al actualizar el plato"})
    }
})
app.delete('/platos/:idPlato', validarId, async (req, res) => {
    let idParams = req.params.idPlato
    try {
        platoEliminado = await schemas.Platos.deleteOne({_id: idParams})
        res.status(200).json({"msg":"Plato eliminado"})
    } catch (error) {
        res.status(200).json({"msg":"Ocurrio un error al eliminar el plato"})
    }

})

app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000")
})