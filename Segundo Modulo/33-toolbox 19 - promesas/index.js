const express = require('express')
const app = express()
const axios = require('axios');
const { response } = require('express');

apiKey = "4d6686f92b58e8dc058da96a1a4ae68d"

citys = [
    {
        name: "Rio grande"
    },
    {
        name: "Ushuaia"
    },
    {
        name: "Tolhuin"
    },
    {
        name: "rio gallegos"
    },
    {
        name: "bariloche"
    },
    {
        name: "calafate"
    },
    {
        name: "epuyen"
    },
    {
        name: "resistencia"
    },

]

async function weather(ciudad){
    const ruta = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${ciudad.name}&appid=${apiKey}&units=metric`)
    const datos = ruta.data.main.temp
    return datos
}

app.get('/weather', async (req, res) => {
    let threeCitys = citys.slice(0, 3)
    let i = 0;
                            //poner async en app.get y ademas poner await en donde agrego los elementos al vector
    while(i < 3){

    const funcion = weather(threeCitys[i])
        .then(respuesta => {
            console.log(respuesta)
            return respuesta
        })

        threeCitys[i].temp = await funcion

        i++;
    }

    res.status(200).json({"ciudades": threeCitys})
    




    /* axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${threeCitys[0].name}&appid=${apiKey}&units=metric`)
    .then(response => {
        return response.data
    })
    .then(data => {
        return data.main
    })
    .then(temp => {
        threeCitys[0].temperatura = temp.temp
        axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${threeCitys[1].name}&appid=${apiKey}&units=metric`)
        .then(response => {
            return response.data
        })
        .then(data => {
            return data.main
        })
        .then(temp => {
            threeCitys[1].temperatura = temp.temp
            axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${threeCitys[2].name}&appid=${apiKey}&units=metric`)
            .then(response => {
                return response.data
            })
            .then(data => {
                return data.main
            })
            .then(temp => {
                threeCitys[2].temperatura = temp.temp
                res.status(200).json({"Weather": threeCitys})
            })
        })
    })
    .catch(error => {
        console.log(error);
    }) */

})

app.listen(3000, ()=>{
    console.log("Corriendo en puerto 3000")
})